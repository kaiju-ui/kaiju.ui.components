import {observer} from "mobx-react";
import React from "react";
import {AsyncPaginate, withAsyncPaginate} from "react-select-async-paginate";
import CreatableSelect from 'react-select/creatable';
import ESelectStore from "./store";
import {LoadingMessage, NoOptions} from "./common";
import "./ESelectComponent.scss"

@observer
export default class ESelectComponent extends React.Component {
    pasteEvent = () => {
    }

    constructor(props) {
        super(props);
        this.store = new ESelectStore({conf: this.props.conf, onChange: props.onChange})

        if (this.store.conf.pasteEvent === true)
            this.pasteEvent = (event) => this.handlePaste(event)

        // функция поднятия стора наверх, тк иначе сброчить значение невозможно
        if (this.props.getStore) {
            this.props.getStore(this.store)
        }

        this.SelectComponent = !this.props.conf.isCreatable
            ? AsyncPaginate
            : withAsyncPaginate(CreatableSelect);
    }

    handleCreate = (inputValue) => {
        const action = {
            action: 'create-option',
            name: 'create-option',
        }
        let vals = this.store.getValue
        let tagsArray = []
        const pushValues = (tags) => {
            inputValue.split('\n').forEach((el) => {
                el.length && tags.push({
                    value: el.trim(),
                    label: el.trim(),
                })
            })
        }
        if (vals == undefined) {
            pushValues(tagsArray)
            this.handleChange(tagsArray, action, undefined)
        } else {
            pushValues(vals)
            this.handleChange(vals, action, undefined)
        }
    }

    handleChange = (vals, actions, e) => {
        this.store.addValue(vals, actions, e)
    }


    handlePaste = (event) => this.handleCreate(event.clipboardData.getData("text"))

    render() {
        const openProps = this.props.conf.menuIsOpen && {
            menuIsOpen: true,
        }


        return (
            <div onPaste={this.pasteEvent}>
                <this.SelectComponent
                    value={this.store.getValue}
                    components={{
                        LoadingMessage: LoadingMessage,
                        NoOptionsMessage: NoOptions
                    }}
                    loadOptions={this.store.loadOptions}
                    defaultOptions={this.props.conf.auto_reload === true}
                    debounceTimeout={this.store.debounceTimeout}
                    isDisabled={this.props.conf.disabled === true || this.props.conf.read_only || this.store.isFetching}
                    isClearable={this.props.conf.isClearable !== false}
                    isMulti={this.props.conf.isMulti || false}
                    onChange={this.handleChange}
                    onCreateOption={this.props.conf.isCreatable && this.handleCreate}
                    closeMenuOnSelect={!(this.props.conf.isMulti || false)}
                    placeholder={""}
                    menuPlacement={this.props.menuPlacement || "auto"}
                    additional={{
                        page: 1,
                    }}
                    {...openProps}
                />
            </div>
        )
    }
}
