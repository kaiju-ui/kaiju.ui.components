// Алиасы нотификации о успешном и ошибочном выполнение действия / запроса
import 'styled-notifications/dist/notifications.css'
import 'styled-notifications/dist/notifications'
import {getTranslation} from "utils";


let notifyWarning = function (title, message, delay) {
    title = title ? title : getTranslation("Notification.error");
    delay = delay ? delay : 3000;
    message = message ? message : "";

    window.createNotification({
        closeOnClick: false,
        displayCloseButton: false,
        positionClass: "nfc-bottom-right",
        showDuration: delay,
        theme: "warning"
    })({
        title: title,
        message: message
    });
};

let notifySuccess = function (title, message, delay) {
    delay = delay ? delay : 1000;

    if (!title) {
        title = getTranslation("Notification.success")
    }

    if (!message) {
        message = ""
    }

    window.createNotification({
        closeOnClick: false,
        displayCloseButton: false,
        positionClass: "nfc-bottom-right",
        showDuration: delay,
        theme: "success"
    })({
        title: title,
        message: message
    });
};

let notifyError = function (title, message, delay) {
    title = title ? title : getTranslation("Notification.error");
    delay = delay ? delay : 3000;
    message = message ? message : "";

    window.createNotification({
        closeOnClick: false,
        displayCloseButton: false,
        positionClass: "nfc-bottom-right",
        showDuration: delay,
        theme: "error"
    })({
        title: title,
        message: message
    });
};

export {notifySuccess, notifyError, notifyWarning}



