import React, {useEffect, useRef} from "react";
import Modal from "./Modal";
import "./Modal.scss"
import {getTranslation} from "utils";

export default class ModalFragment extends React.Component {
    constructor(props) {
        super(props);
        this.confirmCallback = props.confirmCallback;
        this.cancelCallback = props.cancelCallback;
        this.state = {show: this.props.show || false};
    }

    onCancel(event) {
        event.stopPropagation();
        this.cancelCallback ? this.cancelCallback() : undefined;
        this.setState({show: false})
    }

    onConfirm() {
        try {
            this.confirmCallback ? this.confirmCallback() : undefined;
            this.setState({show: false})
        } catch (e) {
            console.log("error - modalFragment.onConfirm: ", e)
        }
    }

    renderHeader() {
        return (
            <React.Fragment>
                <div className="modal-header">
                    <div className={`modal-cancel ${this.props.closeClassName || ''}`}
                         onClick={(event) => this.onCancel(event)}>
                        <i className="icon-cross pointer m--icon-font-size-lg4"/>
                    </div>
                    <div className="modal-ok">
                        {
                            this.confirmCallback && <button
                                type="button"
                                onClick={() => this.onConfirm()}
                                className="btn m-btn--pill btn-success">
                                {getTranslation("Button.confirm")}
                            </button>
                        }
                        {this.props.warningLabel && (
                            <div style={{
                                position: "absolute",
                                right: "2.3rem",
                                width: "400px",
                                textAlign: "right",
                            }}>
                                <i className="icon-warn pr-3"
                                   style={{color: "orange", position: "relative", top: "2px"}}/>
                                {this.props.warningLabel}
                            </div>
                        )}
                    </div>
                    <div className="text-center mr-auto ml-auto p-3">
                        <h1 className="">{this.props.label}</h1>
                        <h5 className="">{this.props.labelInfo}</h5>
                    </div>
                </div>
            </React.Fragment>
        )
    }

    render() {
        return (
            <React.Fragment>
                {
                    this.state.show && <Modal>
                        <div className={`m-portlet window ${this.props.className || ''}`}
                             onClick={event => event.stopPropagation()}>
                            {this.renderHeader()}
                            {this.props.children}
                        </div>
                    </Modal>
                }
            </React.Fragment>
        )
    }

}


class TransparentModalFragment extends ModalFragment {

    render() {
        return (
            <React.Fragment>
                {
                    this.props.show && <Modal>
                        <div
                            className={`window transparent-window ${this.props.className || ''}`}
                            onClick={(event) => event.stopPropagation()}
                        >
                            <div className={`transparent-window-content ${this.props.contentClassName || ''}`}>
                                {this.props.children}
                            </div>
                        </div>
                    </Modal>
                }
            </React.Fragment>
        )
    }
}


const TransparentModal = function ({cancelCallback, show, className, children}) {
    const ModalRef = useRef(null);

    function handleClickOutside(event) {
        if (!show) {
            return
        }

        if (ModalRef && !ModalRef.current.contains(event.target)) {
            cancelCallback()
        }
    }

    useEffect(() => {
        window.addEventListener('mousedown', handleClickOutside);

        return () => {
            window.removeEventListener('mousedown', handleClickOutside)
        }
    }, [ModalRef]);

    const classNames = `m-portlet__body ml-auto mr-auto action-modal mw-500 ${className || ""}`;

    return (
        <React.Fragment>
            {
                show &&
                <TransparentModalFragment show={true}
                                          contentClassName="window-from-content disable-scroll">
                    <div className={classNames}>
                        <div className="row action-modal-content p-5" ref={ModalRef}>
                            <div className="action-modal-cross"
                                 onClick={() => cancelCallback()}>
                                <i className="icon-cross pointer m--icon-font-size-lg4"/>
                            </div>
                            {children}
                        </div>
                    </div>
                </TransparentModalFragment>
            }
        </React.Fragment>
    )
}


export {TransparentModalFragment, TransparentModal}