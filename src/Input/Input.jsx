import React from "react";
import {action, computed, observable} from "mobx";
import ReactTooltip from "react-tooltip";
import "./Input.scss"

class ErrorComponent extends React.Component {
    constructor({id, useTooltip, value}) {
        super();
        this.id = id;
        this.value = value || "";
        this.useTooltip = useTooltip || false;
    }

    render() {
        return (
            <React.Fragment>
                {(this.value && this.value.length > 0) &&
                <React.Fragment>

                    {this.useTooltip &&
                    <ReactTooltip place="right" type="error" effect="solid" id={this.id}>
                        {this.value}
                    </ReactTooltip>
                    }
                    {!this.useTooltip &&
                    <div className="m-form__help text-danger">
                        {this.value}
                    </div>
                    }

                </React.Fragment>
                }
            </React.Fragment>
        )
    }
}

class InputStore {
    error;
    @observable value;

    constructor({value, callback}) {
        this.value = value;
        this.callback = callback
    }

    @action
    onChange(value) {
        this.value = value;
        this.callback(this)
    }

    @computed get hasError() {
        return true
    }

}

class InputComponent extends React.Component {
    constructor(props) {
        super(props);
        let {
            name, placeholder, min, max, step, readonly, className, useTooltip, error, disabled,
            // password flags:
            field_type, new_password = false
        } = props;
        this.disabled = disabled;
        this.errorId = utils.uuid4();
        this.error = error;

        // password flags:
        this.type = field_type || "text";

        if (field_type === "email") {
            this.autocomplete = "email"
        } else if (field_type === "password" ) {
            this.autocomplete = new_password ? "new-password" : "password"
        } else {
            this.autocomplete = "false"
        }

        this.name = name;
        this.placeholder = placeholder || "";
        this.min = min;
        this.max = max;
        this.step = step;
        this.readonly = readonly === true;
        this.className = className;
        this.useTooltip = useTooltip || false;
        this.ref = React.createRef();
        this.store = new InputStore(props)
    }

    componentDidMount() {
        if (this.useTooltip && this.error) {
            ReactTooltip.show(this.ref);
            setTimeout(() => {
                ReactTooltip.hide(this.ref)
            }, 3000)
        }
    }


    render() {
        return (
            <React.Fragment>
                <input
                    disabled={this.disabled}
                    type={this.type}
                    defaultValue={this.store.value}
                    name={this.name}
                    onChange={(e) => this.store.onChange(e.target.value)}
                    placeholder={this.placeholder}
                    autoComplete={this.autocomplete}
                    min={this.min}
                    max={this.max}
                    step={this.step}
                    readOnly={this.readonly}
                    className={`form-control ${this.className || ""}`}
                    data-for={this.errorId}
                    data-tip=""
                    ref={ref => this.ref = ref}
                />
                <ErrorComponent id={this.errorId} value={this.error} useTooltip={this.useTooltip}/>
            </React.Fragment>
        )

    }
}


export default InputComponent;