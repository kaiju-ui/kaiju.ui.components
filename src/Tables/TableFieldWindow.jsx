import {TransparentModal} from "Modal";
import React from "react";
import {action, observable} from "mobx";
import {inject, observer} from "mobx-react";

class TableFieldWindowStore  {
    @observable showModal = false;

    @action
    toggleWindow() {
        this.showModal = !this.showModal;
    }
}

@inject("tableFieldWindowStore")
@observer
export default class TableFieldWindow extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                {
                    this.props.tableFieldWindowStore.showModal &&
                    <TransparentModal
                        show={this.props.tableFieldWindowStore.showModal}
                        cancelCallback={() =>  this.props.tableFieldWindowStore.toggleWindow()}
                    >
                        {this.props.children}
                    </TransparentModal>
                }
            </React.Fragment>
        )
    }
}

export {TableFieldWindowStore, TableFieldWindow}