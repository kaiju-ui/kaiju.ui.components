import React from "react";
import {Link} from "react-mobx-router5";
import ProgressBar from "ProgressBar"
import './extraCell.scss'

function cellWithRefList(pathName, buildParams, className) {
    return (cell) => {

        return (
            <span onClick={e => e.stopPropagation()}>
            {
                (cell?.value || []).map((el, i) => (
                    <React.Fragment key={utils.uuid4()}>
                        <Link
                            routeName={pathName}
                            routeParams={buildParams(el)}
                            className={`active ${className || ""}`}>
                            {el}
                        </Link>
                        {
                            (i < cell.value.length - 1) && <>,&nbsp;</>
                        }
                    </React.Fragment>
                ))
            }
            </span>
        )
    }
}

function cellWithRef(pathName, buildParams, className) {

    return (cell) => {
        return (
            <span onClick={e => e.stopPropagation()}>
                <Link
                    key={cell.value}
                    routeName={pathName}
                    routeParams={buildParams(cell)}
                    className={`withRefA active ${className || ""}`}>
                    {cell.value}
                </Link>
            </span>
        );
    }
}


const progressCell = (cell) => {
    if (!cell.value) {
        return
    }

    return <ProgressBar completed={0 || cell.value?.progress}/>
};

export {cellWithRefList, cellWithRef, progressCell}