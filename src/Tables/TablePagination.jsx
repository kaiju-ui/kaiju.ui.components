import React from 'react'
import PaginationComponent from "./Pagination";
import {inject, observer} from "mobx-react";

@inject("tableStore")
@observer 
class TablePagination extends React.Component {
    render() {
        let page = this.props.tableStore.actions.page;
        let pages = this.props.tableStore.actions.pages;
        
        return (
            <PaginationComponent
                disableCount={this.props.tableStore.disableCount}
                labelCount={this.props.tableStore.conf.labelCount}
                count={this.props.tableStore.actions.count}
                page={page}
                pages={pages}
                className={this.props.className}
                callback={(val) => {
                    this.props.tableStore.actions.stopRefresh()
                    this.props.tableStore.actions.fetchByPage(val)
                    if (this.props.tableStore.paginationCallback) {
                        this.props.tableStore.paginationCallback(val)
                    }
                    this.props.tableStore.actions.startRefresh()
                }}
            />
        )
    }
}

export default TablePagination