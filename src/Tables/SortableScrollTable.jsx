import {inject, observer} from 'mobx-react';
import React from 'react';
import {getTranslation, uuid4} from 'utils';
import {toJS, when} from 'mobx';
import CellComponent from './CellComponent';
import './SortableTable.scss'
import {SortableContainer, SortableElement, SortableHandle} from 'react-sortable-hoc';

@inject('tableStore')
@observer
class HeaderComponent extends React.Component {
    render() {
        return (
            <thead className='thead-inverse'>
            <tr>
                {!this.props.tableStore.disableSort &&
                <th key={uuid4()}/>
                }
                {
                    this.props.tableStore.header.map((el) => {
                        return (<th key={uuid4()}>{el.label}</th>)
                    })
                }
                {
                    this.props.tableStore.ActionsComponent &&
                    this.props.tableStore.header.length > 0 &&
                    <th key={uuid4()}/>
                }
            </tr>
            </thead>
        )
    }
}


@inject('tableStore')
@observer
class BodyComponent extends React.Component {

    renderRow = SortableElement(({rowStore, sortIndex, tableStore}) => {
        let rowRef = React.createRef();

        return (
            <tr className='attribute-row show-hover'
                ref={ref => rowRef = ref}
                onClick={() => {
                    tableStore.rowCallback &&
                    tableStore.rowCallback(rowStore, rowRef)
                }} key={sortIndex}>

                {!tableStore.disableSort && this.DragHandle()}

                {tableStore.header.map((header) => {
                    return <CellComponent
                        key={uuid4()}
                        header={header}
                        row={toJS(rowStore.row)}/>
                })}
                {
                    tableStore.ActionsComponent &&
                    <td className='v-align-center'
                        onClick={(e) => (e.stopPropagation())}>
                        <tableStore.ActionsComponent
                            rowStore={rowStore}
                            rowRef={rowRef}
                            tableStore={tableStore}/>
                    </td>
                }
            </tr>
        )
    });

    DragHandle() {
        let DragComp = SortableHandle(() =>
            <i className='icon-arrows draggable'/>);
        return (
            <td className='sort-table__icon' onClick={(event) => event.stopPropagation()}>
                <DragComp/>
            </td>
        )
    };

    renderBody = SortableContainer(({children}) => {
        return <tbody className='fgroup-collapsed'>
        {children}
        </tbody>
    });

    render() {
        return (
            <this.renderBody
                useDragHandle
                onSortEnd={({oldIndex, newIndex}) => {
                    this.props.tableStore.actions.updateSort(oldIndex, newIndex);
                }}
                helperClass='sortableHelper'
                lockAxis={'y'}>
                {
                    this.props.tableStore.data.map((rowStore, index) => {
                            return <this.renderRow
                                key={index}
                                rowStore={rowStore}
                                tableStore={this.props.tableStore}
                                index={index}
                                sortIndex={index}/>
                        }
                    )
                }
            </this.renderBody>
        )
    }
}

const CountElement = observer(({tableStore}) => {
    let count = tableStore.actions.count;
    return (
        <div className='sort-table__count'>
            {tableStore.actions.count > 0 &&
            <div className='grey'>
                {`${getTranslation('Count')}: ${count}`}
            </div>
            }
        </div>
    )
});

@inject('tableStore')
@observer
export default class SortableScrollTable extends React.Component {
    pixelsToGo = 50;
    oneColumnHeight = 42;

    validateHeight() {
        if (this.props.height > ((this.perPage * this.oneColumnHeight) + this.pixelsToGo)) {
            console.warn("Wrong height params for table: ", this.props.height)
        }
    }

    constructor(props) {
        super(props);
        this.className = this.props.className || '';
        this.params = this.props.tableStore.conf.requests.load.params || {};
        this.perPage = this.params.per_page || 24;

        this.ref = React.createRef();
        this.validateHeight();
        when(
            () => this.props.tableStore.actions.isFetching === false,
            this.tryFetchMore
        )
    }

    // Когда высота элементов меньше чем высота контейнера
    // фетчим данные при скролле
    tryFetchMore = () => {
        const elemsHeight = this.props.tableStore.data.length * this.oneColumnHeight;
        const isFetch = this.ref.current.offsetHeight > elemsHeight;
        if (isFetch) {
            this.props.tableStore.actions.fetchNext();
        }
    }
    componentDidMount() {
        this.ref.current.addEventListener('wheel', this.tryFetchMore);
    }
    componentWillUnmount() {
        this.ref.current.removeEventListener('wheel', this.tryFetchMore);
    }

    tableClasses() {
        if (!this.props.showLoader) {
            return ""
        }

        if (this.props.tableStore.actions.isFetching === true
            || this.props.tableStore.actions.page === 0) {
            return "table-loader"
        }
        return ""
    }

    render() {
        return (
            <div id={this.props.id || ''} className={this.className}>
                {!this.props.tableStore.disableCount && <CountElement tableStore={this.props.tableStore}/>}
                <div
                    style={this.props.style || {}}
                    className={this.tableClasses()}>
                    <div className='sort-table'
                         style={{height: `${this.props.height}px`}}
                         onScroll={e => this.scrollEvent(e)}
                         ref={this.ref}
                    >
                        <table className='table table-hover table-checkable dataTable no-footer' id={uuid4()}>
                            {!this.props.tableStore.conf.disableHeader && <HeaderComponent/>}
                            <BodyComponent/>
                        </table>
                    </div>

                </div>
            </div>
        )
    }

    scrollEvent(event) {
        const offsetTop = event.target.offsetHeight + event.target.scrollTop;
        const lowerLimit = event.target.scrollHeight - this.oneColumnHeight;

        if (offsetTop >= lowerLimit) {
            this.props.tableStore.actions.fetchNext();
        }
    }

}


