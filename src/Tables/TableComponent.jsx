import {inject, observer} from "mobx-react";
import React from "react";
import {uuid4} from "utils";
import {toJS} from "mobx";
import TablePagination from "./TablePagination";
import CellComponent from "./CellComponent";
import CheckboxComponent from "Checkbox";
import "./TableComponent.scss"
import RowStore from "./stores/rowStore";

@inject("tableStore")
@observer
class HeaderComponent extends React.Component {

    render() {
        return (
            <thead className="thead-inverse">
            <tr>
                {
                    this.props.tableStore.checkable &&
                    <td className="product-table-checkbox" key={uuid4()} onClick={(e) => (e.stopPropagation())}>
                        {this.props.tableStore.checkMulti &&
                            <
                                CheckboxComponent
                                onChange={value => {
                                    this.props.tableStore.actions.toggleCheckedAllPage(value);
                                }}

                                defaultChecked={this.props.tableStore.actions.allPageChecked || false}
                            />
                        }
                    </td>
                }

                {this.props.tableStore.nestedMode && <td></td>}

                {
                    this.props.tableStore.header.map((el) => {
                        return (<th key={uuid4()}>{el.label}</th>)
                    })
                }

                {
                    this.props.tableStore.ActionsComponent &&
                    this.props.tableStore.header.length > 0 &&
                    <th key={uuid4()}/>
                }
            </tr>
            </thead>
        )
    }
}

@inject("tableStore")
@observer
class GroupHeaderComponent extends React.Component {
    render() {
        let {groupStore, tableStore} = this.props;
        let colSpan = tableStore.header.length + (this.props.tableStore.ActionsComponent ? 1 : 0);

        return (
            <tr onClick={() => groupStore.toggleGroup()}>
                <td colSpan={colSpan} className="group-header">
                    <i className={!groupStore.isToggled ? "icon-square_minus" : "icon-square_plus"}
                       style={{fontSize: "12px"}}
                    />
                    <h5 style={{display: "inline-block"}} className="pl-2">
                        {groupStore.label}
                    </h5>
                </td>
            </tr>
        )
    }
}


@observer
class TableBodyComponent extends React.Component {
    render() {
        // Accordion - если accordion=True придет GroupStore
        let {groupStore = {}} = this.props;

        return (
            <tbody className={groupStore.isToggled ? "group-collapsed" : ""}>
            {
                (groupStore.label && groupStore.label.length) &&
                <GroupHeaderComponent key={uuid4()} groupStore={groupStore}/>
            }
            {this.props.children}
            </tbody>

        )
    }
}

@observer
class RowCheckboxComponent extends React.Component {
    render() {
        const {rowStore} = this.props;

        if (this.props.nested) {
            return <td className="product-table-checkbox" key={uuid4()}/>
        }

        return (
            <td className="product-table-checkbox" key={uuid4()}
                onClick={(e) => {
                    e.stopPropagation();
                    rowStore.toggleChecked()
                }}>
                <CheckboxComponent
                    onChange={() => {
                        rowStore.toggleChecked();
                    }}
                    defaultChecked={rowStore.checked || false}/>
            </td>
        )
    }
}

/*
in the table rpc response:

pagination...
data: ...,
count: ...
footer: {
  cell2: 300,
  cell5: 500
}  - it uses default table cell constructor and header to build footer.

 */
@inject("tableStore")
class TableFooter extends React.Component {

    renderFooterCell(header, i) {
        const field = header["key"];
        let value = this.props.tableStore.footer[field];

        if (i === 0) {
            return <td className="v-align-center" key={uuid4()}>
                {utils.getTranslation("Footer.total")}
            </td>
        }

        let defaultConstructor = this.props.tableStore.cellConstructors["default"];
        value = defaultConstructor.bind(this)({value: value});
        return (
            <td className="v-align-center" key={uuid4()}>
                {value}
            </td>
        )
    }

    render() {
        return (
            <tfoot>
            <tr>
                {this.props.tableStore.checkable && <td/>}
                {this.props.tableStore.header.map((header, i) => this.renderFooterCell(header, i))}
            </tr>
            </tfoot>
        )
    }
}

class NestedCollapse extends React.Component {
    render() {
        return (
            <td className={`model-collapse ${this.props.hasNested ? "pointer" : ""}`}
                key={uuid4()}
                onClick={e => {
                    e.stopPropagation();
                    this.props.hasNested && this.props.callback()
                }}>
                {
                    this.props.hasNested &&
                    <i className={`icon-collapse pointer ${!this.props.nested ? "rotate-icon" : ""}`}/>
                }
            </td>
        )
    }
}


@inject("tableStore")
class RowComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {nested: this.props.nestedMode}
        this.nestedRows = this.props.rowStore.data["@nested"]?.value || [];
        this.hasNested = this.nestedRows.length > 0;
        this.nestedClass = this.hasNested ? "table__nested-model" : ""

    }

    collapseCallback() {
        this.setState({nested: !this.state.nested})
    }

    render() {

        return (
            <>
                <tr className={`attribute-row show-hover ${this.props.className || ""}  ${this.nestedClass}`}
                    onClick={() => {
                        this.props.tableStore.rowCallback &&
                        this.props.tableStore.rowCallback(this.props.rowStore)
                    }} key={uuid4()}>

                    {
                        this.props.checkable &&
                        <RowCheckboxComponent
                            nested={this.props.nested}
                            rowStore={this.props.rowStore}/>
                    }

                    {
                        this.props.tableStore.nestedMode && (
                            <NestedCollapse
                                nested={this.state.nested}
                                hasNested={this.hasNested}
                                callback={() => this.collapseCallback()}
                            />
                        )
                    }


                    {
                        this.props.tableStore.header.map((header) => {
                            return (
                                <CellComponent
                                    key={uuid4()}
                                    header={header} row={toJS(this.props.rowStore.row)}
                                />
                            )
                        })
                    }

                    {
                        this.props.tableStore.ActionsComponent &&
                        <td className="v-align-center"
                            onClick={(e) => (e.stopPropagation())}>
                            <this.props.tableStore.ActionsComponent
                                rowStore={this.props.rowStore}
                                tableStore={this.props.tableStore}/>
                        </td>
                    }
                </tr>

                {this.state.nested &&
                    this.nestedRows.map((row, i) =>
                        (
                            <RowComponent
                                className={`table__nested ${i === 0 ? "table__nested-first" : ""}`}
                                nested={true}
                                checkable={true}
                                rowStore={new RowStore(row, this.props.tableStore)}
                                tableStore={this.props.tableStore}
                                key={uuid4()}/>
                        )
                    )
                }
            </>
        )
    }
}

@inject("tableStore")
class TableBody extends React.Component {


    renderRows(rows) {
        // rows в tableStore-e заворачиваются в observable map
        // в каждом элементе rows находится RowStore
        return (
            <React.Fragment>
                {rows.map((rowStore) => {
                        return (
                            <RowComponent
                                checkable={this.props.tableStore.checkable}
                                key={uuid4()}
                                rowStore={rowStore}
                            />
                        )
                    }
                )}
            </React.Fragment>
        )
    }

    render() {
        return (
            <React.Fragment>
                {
                    this.props.tableStore.data.map((group) => {
                        // group - groupStore при accordion=True
                        return (
                            <TableBodyComponent groupStore={group} key={uuid4()}>
                                {this.renderRows(group.rows)}
                            </TableBodyComponent>
                        )
                    })
                }
            </React.Fragment>
        )
    }
}


@inject("tableStore")
@observer
export default class MainTableComponent extends React.Component {
    constructor(props) {
        super(props);
        this.className = this.props.className || "";
        this.paginationClassName = this.props.paginationClassName || "";
    }

    componentDidMount() {
        this.props.tableStore.actions.startRefresh()
    }

    componentWillUnmount() {
        this.props.tableStore.actions.stopRefresh()
    }

    render() {
        return (
            <React.Fragment>
                {
                    !this.props.tableStore.disablePagination &&
                    <TablePagination className={this.paginationClassName}/>
                }
                <div className={
                    this.props.tableStore.actions.isFetching === true
                    || this.props.tableStore.actions.page === 0 ? `table-loader ${this.props.className}` : this.props.className
                }>
                    <table className="table table-hover table-checkable dataTable no-footer" id={uuid4()}>
                        {!this.props.tableStore.conf.disableHeader && <HeaderComponent/>}
                        <TableBody/>
                        {this.props.tableStore.footer && <TableFooter/>}
                    </table>
                </div>
            </React.Fragment>
        )
    }
}


class TableDropdowns extends React.Component {

    render() {
        return (
            <div className={`table-dropdowns ${this.props.className || ""}`}>
                {this.props.children}
            </div>
        )
    }

}

export {
    TableDropdowns
}