import CheckboxComponent from "Checkbox";
import React from 'react'
import axios from 'axios';
import {notifyError, notifySuccess} from "../notifications";
import {getPhoto, getTranslation, uuid4} from "utils";
import ReactJson from 'react-json-view'
import {TableFieldWindow, TableFieldWindowStore} from "./TableFieldWindow";
import {Provider} from "mobx-react";

const defaultCell = ({value, behavior = undefined}) => {

    if (!behavior || value === null) {
        return (
            <React.Fragment>
                {value}
            </React.Fragment>
        )
    }

    switch (behavior) {
        case "list":
            if (Array.isArray(value)) {
                value = value.join(", ")
            }

            return (
                <React.Fragment>
                    {value}
                </React.Fragment>
            );
        case "range":
            let formatted;

            if ((value.max || value.max === 0) && value.min || value.min === 0) {
                formatted = `${value.min} - ${value.max}`
            } else if ((value.max || value.max === 0)) {
                formatted = `${getTranslation("To")} ${value.max}`
            } else if (value.min || value.min === 0) {
                formatted = `${getTranslation("From")} ${value.min}`
            }

            return (
                <React.Fragment>
                    {formatted}
                </React.Fragment>
            )
        default:
            console.warn("TypeError: behavior type not found.");
            return (
                <React.Fragment>
                    {toString(value)}
                </React.Fragment>
            )
    }


};

const photo = (cell) => {
    let src;

    src = getPhoto(cell);

    let imgStyle = {
        "height": "50px",
        "width": "50px"
    };


    return (
        <div style={{width: "70px"}}>
            {
                src &&
                <img style={imgStyle}
                    // # TODO при ховере делать картинку больше
                     alt="product"
                     src={src}
                />
            }
        </div>
    )
};


const document = (cell) => {
    return (
        <div className="row" onClick={(event) => event.stopPropagation()}>
            {cell.value &&
                <React.Fragment>
                    {cell.value.map(value => {
                        return (
                            <a href={value.path} download={value.name}
                               style={{position: "relative", textAlign: "center"}}
                               className="col-2" key={uuid4()}>
                            <span className="m-nav__link-badge m-badge m-badge--accent"
                                  style={{position: "absolute", left: "35px", top: "14px", fontSize: "20px"}}>
                                <i className="icon-download-cloud"/>
                            </span>
                                <i className="icon-file-preview text-body" style={{fontSize: "45px"}}/>
                            </a>

                        )
                    })}
                </React.Fragment>
            }
        </div>
    )
};


const document_link = (cell) => {
    return (
        <div>
            {cell.value &&
                <div style={{position: "relative"}}>
            <span className="m-nav__link-badge m-badge m-badge--accent"
                  style={{position: "absolute", left: "20px", top: "14px", fontSize: "20px"}}>
                <i className="icon-download-cloud"/></span>
                    <i className="icon-file-preview text-body" style={{fontSize: "45px"}}/>

                </div>
            }

        </div>
    )
};


const video = (cell) => {
    return (
        <div className="row" onClick={(event) => event.stopPropagation()}>
            {cell.value &&
                <React.Fragment>
                    {cell.value.map(value => {
                        return (
                            <a href={value.path} download={value.name}
                               style={{position: "relative", textAlign: "center"}}
                               className="col-2" key={uuid4()}>
                            <span className="m-nav__link-badge m-badge m-badge--accent"
                                  style={{position: "absolute", left: "35px", top: "14px", fontSize: "20px"}}>
                                <i className="icon-video"/>
                            </span>
                                <i className="icon-file-preview text-body" style={{fontSize: "45px"}}/>
                            </a>

                        )
                    })}
                </React.Fragment>
            }
        </div>
    )
};


const boolean = (cell) => {
    return (
        <div style={{width: "70px"}}>
            {cell.value &&
                <i className="icon-checked" style={{fontSize: "18px"}}/>
            }
        </div>
    )
};

let checkboxOnChange = (cell, value) => {
    cell.value.params["value"] = value;

    let requestData = {
        "method": cell.value.method,
        "params": cell.value.params
    };

    axios.post("/public/rpc", requestData)
        .then(function (response) {
            notifySuccess(getTranslation("Notification.changed"));
        })
        .catch(function (error) {
            notifyError(null, error.response.data.error.message);
        }.bind(this));

};

const checkbox = (cell) => {

    return <CheckboxComponent
        defaultChecked={cell.value.value}
        onChange={checked => checkboxOnChange(cell, checked)}/>;
};

const toggle = (cell) => {
    // TODO:
    // let comp = new BooleanComponent({
    //     displayLabel: false,
    //     value: cell.value.value,
    //     disabled: cell.disabled, // TODO: permissions!
    //     onChange: function () {
    //         cell.value.params["value"] = this.value;
    //
    //         let requestData = {
    //             "method": cell.value.method,
    //             "params": cell.value.params
    //
    //         };
    //
    //         axios.post(ROUTES.rpc, requestData)
    //             .then(function (response) {
    //                 notifySuccess("Изменено");
    //             })
    //             .catch(function (error) {
    //                 this.setValue(!this.value);
    //                 notifyError(null, error.response.data.error.message);
    //             }.bind(this));
    //     }
    // });
    //
    // return comp.element
};

const object = ({value}) => {
    return (
        <div style={{cursor: "auto", padding: "4px"}}
             onClick={(e) => e.stopPropagation()}>
            <pre style={{whiteSpace: "pre-wrap", overflowWrap: "break-word"}}>
                {JSON.stringify(value, null, 2)}
            </pre>
        </div>
    )
};


class JsonObjectView extends React.Component {
    constructor(props) {
        super(props);
        this.tableFieldWindowStore = new TableFieldWindowStore()
    }

    render() {
        let previewValue;

        let {id, key, value} = this.props.cell

        if (typeof this.props.handleValue == 'function') {
            previewValue = this.props.handleValue(value)
        } else {
            previewValue = "{...}"
        }

        return (
            <>
                <a
                    className="pointer text-center"
                    onClick={(event) => {
                        event.stopPropagation();
                        this.tableFieldWindowStore.toggleWindow()
                    }}
                >
                    <pre>{previewValue}</pre>
                </a>
                <Provider tableFieldWindowStore={this.tableFieldWindowStore}>
                    <TableFieldWindow>
                        <div className="w-100">
                            {
                                this.props.cell.id && (
                                    <h3 className="m-nav-grid__text text-center mb-5">
                                        {id} | {key}
                                    </h3>
                                )
                            }
                            <ReactJson src={value} name={null}/>
                        </div>
                    </TableFieldWindow>
                </Provider>
            </>
        )
    }
}

const jsonObject = (cell, handleValue) => {
    if (!cell.value) {
        return
    }

    return (
        <div>
            <JsonObjectView cell={cell} handleValue={handleValue}/>
        </div>
    )
};

class TaskResultJsonObjectView extends React.Component {
    constructor(props) {
        super(props);
        this.tableFieldWindowStore = new TableFieldWindowStore()
    }

    parseValue(key) {
        if (Object.prototype.toString.call(key) === '[object Array]') {
            return key.length
        }


        return JSON.stringify(key).slice(0, 15)
    }

    writeShortResult({value}) {
        let result

        try {
            result = value[0]['RESULT']?.result
            result = result || value[0]['RESULT']?.error
        } catch (e) {
            result = value[0]['CMD']
            console.error(e)
            result = "..."
        }

        if (typeof (result) === "object") {
            return (
                <ul>
                    {
                        Object.keys(result).map((key) => {
                            return (
                                <li key={key}>
                                    {key}: <b>{this.parseValue(result[key])}</b>
                                </li>
                            )
                        })
                    }
                </ul>
            )
        }

        if (typeof (result === "string")) {
            return <span>{JSON.stringify(result || "").slice(0, 15)}</span>
        }

        return (
            <span>{result}</span>
        )
    }

    render() {

        return (
            <>
                <a
                    className="pointer"
                    onClick={(event) => {
                        event.stopPropagation();
                        this.tableFieldWindowStore.toggleWindow()
                    }}
                >
                    <pre>{this.writeShortResult(this.props.cell)}</pre>
                </a>
                <Provider tableFieldWindowStore={this.tableFieldWindowStore}>
                    <TableFieldWindow>
                        <div className="w-100">
                            {
                                this.props.cell.id && (
                                    <h3 className="m-nav-grid__text text-center mb-5">
                                        {this.props.cell.id} | {this.props.cell.key}
                                    </h3>
                                )
                            }
                            <ReactJson src={this.props.cell.value} name={null}/>
                        </div>
                    </TableFieldWindow>
                </Provider>
            </>
        )
    }
}


const taskResultJsonObject = (cell) => {
    if (!cell.value) {
        return
    }

    return (
        <div>
            <TaskResultJsonObjectView cell={cell}/>
        </div>
    )
};

const tableCellConstructors = {
    default: defaultCell,
    date: defaultCell,
    datetime: defaultCell,
    photo: photo,
    video: video,
    boolean: boolean,
    checkbox: checkbox,
    document: document,
    document_link: document_link,
    toggle: toggle,
    object: object,
    json_object: jsonObject
};


const tableFieldConstructors = {
    complete: ({value}) => {
        return (
            <React.Fragment>
                {
                    value !== null &&
                    <i className="m-badge m-badge--info m-badge--wide m-badge--rounded">
                        <b>
                            {`${value}%`}
                        </b>
                    </i>
                }
            </React.Fragment>

        )
    },
    synonyms: ({value}) => {
        return (
            <React.Fragment>
                <ul>
                    {
                        value.map((v, i) => <li key={i}>{v}</li>)
                    }
                </ul>
            </React.Fragment>
        )
    },
    task_result: taskResultJsonObject

};

export default tableCellConstructors
export {tableFieldConstructors}