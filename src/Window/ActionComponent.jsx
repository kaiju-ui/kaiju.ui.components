import ModalFragment from "Modal/ModalFragment";
import React from "react";
import {getTranslation, Timer} from "../utils";
import {action} from "mobx";
import {observer} from "mobx-react";


export default class WindowActionComponent extends React.Component {
    cancelLabel = getTranslation("Button.cancel"); // Отмена
    confirmLabel = getTranslation("Button.confirm"); // Подтвердить
    confirmText = getTranslation("Action.confirm"); // Подтвердите ваше действие
    confirmButtonClass = "btn-default"; // Класс кнопки

    // Указывается класс картинки ИЛИ иконки
    iconClass; // Класс иконки
    imageHref; // Класс картинки

    actionTime = 3

    constructor(props) {
        super(props);
        this._startCallback = this.props.startCallback; // к примеру, чтобы можно было остановить autorefresh
        this.cancelCallback = this.props.cancelCallback;
        this.actionCallback = this.props.actionCallback;
        this.idLabel = this.props.idLabel;  // Id атрибута
        this.actionLabel = this.props.actionLabel; // Удаление атрибута
        this.state = {
            show: this.props.show
        };

        this.cancelLabel = this.props.cancelLabel || this.cancelLabel;
        this.confirmLabel = this.props.confirmLabel || this.confirmLabel;
        this.confirmText = this.props.confirmText || this.confirmText;
        this.confirmButtonClass = this.props.confirmButtonClass || this.confirmButtonClass
        this.withActionTimeout = this.props.withActionTimeout

        this.confirmCallback = () => {
            this.actionCallback ? this.actionCallback() : undefined;
            this.setState({show: false});
        };

        this.timer = new Timer({
            count: this.actionTime,
            callback: this.confirmCallback
        })
    }

    startCallback() {
        if (this._startCallback) {
            this._startCallback()
        }

        this.setState({show: true})
    }

    renderButton() {
        if (this.props.disableButton) {
            return <React.Fragment/>
        }

        if (this.props.children) {
            return <span onClick={() => this.startCallback()}>
                {this.props.children}
            </span>
        } else if (this.iconClass) {
            return <i
                onClick={() => this.startCallback()}
                className={`${this.iconClass} pointer`} style={{fontSize: "18px"}}/>
        } else {
            return <React.Fragment/>
        }
    }

    confirmButton = observer(() => {
        return (
            <button
                type="button"
                disabled={this.timer.isStarted}
                onClick={() => this.onDelete()}
                className={`btn m-btn--pill mr-3 ${this.confirmButtonClass}`}
            >
                {this.confirmLabel}
                {this.timer.isStarted && <span>{` (${this.timer.count})`}</span>}
            </button>
        )
    })

    onCancel() {
        this.withActionTimeout && this.timer.stop()
        this.cancelCallback ? this.cancelCallback() : undefined;
        this.setState({show: false});
    }

    @action
    onDelete() {
        if (this.withActionTimeout) {
            this.timer.start()
        } else {
            this.confirmCallback()
        }
    }

    render() {

        return (
            <React.Fragment>
                {this.renderButton()}
                {
                    (this.state.show || this.props.show) &&
                    <ModalFragment show={this.state.show || this.props.show}
                                   cancelCallback={() => this.onCancel()}>
                        <div className="m-portlet__body ml-auto mr-auto action-modal">
                            <div className="action-modal__notification row">
                                <div className="action-modal__icon col-4">
                                    <div className="border-right pr-3"
                                         style={{float: "right", minWidth: "200px", height: "320px"}}>
                                        {
                                            this.iconClass &&
                                            <i className={this.iconClass} style={{fontSize: "200px"}}/>
                                        }
                                        {
                                            this.imageHref &&
                                            <img src={this.imageHref} alt="" style={{height: "320px"}}/>
                                        }
                                    </div>
                                </div>

                                <div className="action-modal__message col-8">
                                    <div style={{float: "left", height: "250px"}}>
                                        <h3 className="pt-3">{this.confirmText}</h3>
                                        {
                                            this.actionLabel && <h2 style={{wordBreak: "breakAll"}}>
                                                {this.actionLabel}
                                            </h2>
                                        }
                                        {
                                            this.idLabel && <h2 style={{wordBreak: "breakAll"}}>
                                                {this.props.noBreakets && this.idLabel}
                                                {!this.props.noBreakets && `[${this.idLabel}]`}
                                            </h2>
                                        }

                                        <div className="pb-3"
                                             style={{position: "absolute", bottom: 0}}
                                        >

                                            <button type="button"
                                                    onClick={() => this.onCancel()}
                                                    className="btn m-btn--pill mr-3 btn-metal"

                                            >{this.cancelLabel}
                                            </button>
                                            <this.confirmButton/>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </ModalFragment>
                }
            </React.Fragment>
        )
    }
}

