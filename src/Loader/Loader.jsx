import React, {useEffect} from "react";
import "./Loader.scss"

const Loader = function ({show = true}) {

    useEffect(() => {
        return () => {
            window.body.classList.remove("body-loader")
        }
    });

    if (show) {
        window.body.classList.add("body-loader")
    } else {
        window.body.classList.remove("body-loader")
    }

    return (
        <React.Fragment/>
    )
};

export default Loader