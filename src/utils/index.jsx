import {handleNotifyExceptions, parseException} from "utils/exceptions";
import {action, observable} from "mobx";

const translations = {
    "To": "до",
    "From": "от",
    "Notification.changed": "изменено",
    "Button.confirm": "Подтвердить",
    "Button.cancel": "Отмена",
    "Action.confirm": "Подтвердить действие",
    "Count": "Количество",
    "Placeholder.search": "Поиск"
};

String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

const getTranslation = (code, capitalize = true) => {
    if (translations[code]) {
        return capitalize ? translations[code].capitalize() : translations[code]
    }

    return `[${code}]`;
};

const uuid4 = function () {
    //// return uuid of form xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx
    var uuid = '', ii;
    for (ii = 0; ii < 32; ii += 1) {
        switch (ii) {
            case 8:
            case 20:
                uuid += '-';
                uuid += (Math.random() * 16 | 0).toString(16);
                break;
            case 12:
                uuid += '-';
                uuid += '4';
                break;
            case 16:
                uuid += '-';
                uuid += (Math.random() * 4 | 8).toString(16);
                break;
            default:
                uuid += (Math.random() * 16 | 0).toString(16);
        }
    }
    return uuid
};

const getPhoto = (data) => {
    let path;

    if (data.value && data.value.original) {
        path = data.value.tiny ? data.value.tiny.path : data.value.original.path;
    }

    return path
};

class Timer {
    @observable count;
    @observable isStarted = false;

    timeout;

    constructor({count, callback}) {
        if (count < 0) {
            throw Error;
        }

        this.count = count;
        this.initialCount = count;
        this.callback = callback
    }

    @action.bound
    start() {
        this.isStarted = true;
        let self = this;
        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            let timeHasCome = self.decreaseCount()

            if (timeHasCome) {
                self.callback()
                this.stop()
            } else {
                self.start()
            }
        }, 1000)
    }

    @action.bound
    decreaseCount() {
        this.count = this.count - 1
        return this.count < 0
    }

    @action.bound
    stop() {
        clearTimeout(this.timeout)
        this.isStarted = false
        this.count = this.initialCount
    }
}

export {
    getPhoto,
    getTranslation,
    uuid4,
    parseException,
    handleNotifyExceptions,
    Timer
}
export default uuid4;


