import {observer} from "mobx-react";
import React from "react";
import {Helmet} from "react-helmet";

const MetaTitle = observer(({breadcrumbs}) => {
    let _breadcrumbs = breadcrumbs.map(el => el.label).reverse()

    return (
        <>
            <Helmet>
                <title>{_breadcrumbs.join(" | ")}</title>
            </Helmet>
        </>
    )
})

export default MetaTitle