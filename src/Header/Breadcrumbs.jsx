import React from 'react'
import {inject} from "mobx-react";


@inject("routerStore")
export default class Breadcrumbs extends React.Component {

    renderItem(el) {
        return <span className={el.path ? "pointer" : "grey"} onClick={e => {
            el.path && this.props.routerStore.router.navigate(el.path, el.params);
        }}>
            {el.label}
        </span>

    }

    render() {
        return (
            <React.Fragment>
                {this.props.crumbs.map((el, index, obj) => {
                    return (
                        <React.Fragment key={utils.uuid4()}>
                            {this.renderItem(el)}

                            {(index === 0 || index + 1 < obj.length) &&
                            <span className="pr-2 pl-2">/</span>
                            }

                        </React.Fragment>)
                })}
            </React.Fragment>
        )
    }
}