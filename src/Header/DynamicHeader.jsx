import {observer} from "mobx-react";
import Header from "./base";
import React from "react";

/**
 * Хедер используется для страниц редактирования и создания в настройках
 */
@observer
class DynamicHeader extends React.Component {


    render() {
        let {isFetching = false, isChanged = false, label, buttonConf, doubleButtonConf, breadcrumbs} = this.props;

        const disabled = isFetching || isChanged !== true;
        const labelInfo = isChanged ? utils.getTranslation("Message.unsaved_changes") : undefined;

        buttonConf = buttonConf ? {
            ...buttonConf,
            disabled: disabled,
            labelInfo: labelInfo
        } : null;

        doubleButtonConf = doubleButtonConf ? {
            ...doubleButtonConf,
            mainButtonConf: {
                ...doubleButtonConf.mainButtonConf,
                disabled: disabled
            },
            labelInfo: labelInfo
        } : null;

        return (
            <Header
                className={this.props.className}
                breadcrumbs={breadcrumbs}
                buttonConf={buttonConf}
                doubleButtonConf={doubleButtonConf}
            >
                {
                    label &&
                    <span className="m--icon-font-size-lg3 m--font-bolder">
                    {label}
                    </span>
                }
            </Header>
        )
    }
}

export default DynamicHeader