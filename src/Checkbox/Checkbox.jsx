import React from 'react'
import "./Checkbox.scss"

export default class CheckboxComponent extends React.Component {
    // Обычный чекбокс компонент

    render() {
        let classNames = `
        ${this.props.indeterminate ? "m-checkbox-indeterminate " : ""}
         m-checkbox m-checkbox--primary m-checkbox--bold pb-2 ml-2 no-select
         ${this.props.disabled ? "m-checkbox-disabled" : ""}
         ${this.props.className || ""}`;

        return (
            <label
                className={classNames}>
                <input
                    disabled={this.props.disabled}
                    onChange={event => this.props.onChange(event.target.checked, this.props.indeterminate)}
                    defaultChecked={this.props.controled ? undefined : this.props.defaultChecked}
                    checked={this.props.controled ? this.props.value : undefined}
                    type="checkbox"/>
                {this.props.label && this.props.label}
                <span className="inner-checkbox-span"/>
            </label>
        )
    }
}